from src.enums import BodyPart
from src.exercise import (
    Exercise,
    Lift,
    Workout,
    get_total_sets,
    get_total_volume,
)


def test_exercise():
    ohp = Exercise("OHP", bp=[BodyPart.SHOULDERS, BodyPart.TRICEPS])
    tex = Exercise("tricep extensions", bp=[BodyPart.TRICEPS])
    print(ohp)
    print(tex)


def test_lift():
    ohp = Exercise("OHP", bp=[BodyPart.SHOULDERS, BodyPart.TRICEPS])

    l1 = Lift(ohp, 135, 3, 10)
    l2 = Lift(ohp, 25, 14, 20)

    assert l1.volume == 30
    assert l2.volume == 280

    assert l1.volume_dict == {BodyPart.SHOULDERS: 30, BodyPart.TRICEPS: 30}


def test_workout():
    ohp = Exercise("OHP", bp=[BodyPart.SHOULDERS, BodyPart.TRICEPS])
    tex = Exercise("tricep extensions", bp=[BodyPart.TRICEPS])

    lohp = Lift(ohp, 135, 3, 10)
    ltex = Lift(tex, 135, 4, 8)

    w = Workout([lohp, ltex])
    assert get_total_volume(w) == {
        BodyPart.SHOULDERS: 30,
        BodyPart.TRICEPS: 62,
    }
    assert get_total_sets(w) == {BodyPart.SHOULDERS: 3, BodyPart.TRICEPS: 7}
