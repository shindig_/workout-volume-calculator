from src.enums import BodyPart
from src.exercise import Exercise, Lift, Workout, get_total_sets
from rich import print

overhead_press = Exercise(
    "OHP", [BodyPart.FRONT_DELT, BodyPart.SIDE_DELT, BodyPart.TRICEPS]
)
tricep_ext = Exercise("tricep extensions", [BodyPart.TRICEPS])
db_ohp = Exercise(
    "db ohp", [BodyPart.FRONT_DELT, BodyPart.SIDE_DELT, BodyPart.TRICEPS]
)
bb_rows = Exercise("barbell rows", [BodyPart.BACK])


def main():
    monday = Workout(
        [
            Lift(overhead_press, 115, 3, 6),
            Lift(bb_rows, 185, 2, 10),
            Lift(db_ohp, 30, 3, 10),
            Lift(tricep_ext, 30, 3, 8),
        ]
    )

    print(monday.total_volume_dict)
    print(get_total_sets(monday))


if __name__ == "__main__":
    main()
