from src.enums import BodyPart
from dataclasses import dataclass
from typing import Dict, List
from collections import defaultdict


@dataclass
class Exercise:
    name: str
    bp: List[BodyPart]


@dataclass
class Lift:
    exercise: Exercise
    weight: int
    sets: int
    reps: int

    @property
    def volume(self) -> int:
        return self.sets * self.reps

    @property
    def volume_dict(self) -> Dict[BodyPart, int]:
        return {e: self.volume for e in self.exercise.bp}


@dataclass
class Workout:
    lifts: List[Lift]


def get_total_volume(workout: Workout) -> Dict[BodyPart, int]:
    out = defaultdict(int)
    for lift in workout.lifts:
        for bp, vol in lift.volume_dict.items():
            out[bp] += vol
    return out


def get_total_sets(workout: Workout) -> Dict[BodyPart, int]:
    out = defaultdict(int)
    for lift in workout.lifts:
        for body_part in lift.exercise.bp:
            out[body_part] += lift.sets
    return out
