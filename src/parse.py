from shutil import ExecError
import yaml
from src.exercise import Exercise, Lift, Workout, get_total_sets
from src.enums import bodypart_dict
import rich
from typing import Any, Dict, List, Union


def load(file_path: str) -> Union[Dict, List]:
    with open(file_path, "r") as F:
        return yaml.safe_load(F.read())


def get_exercise_dict(contents: str) -> Dict[str, Exercise]:
    exercise_dict = {}
    for name, el in contents.items():
        exercise_dict[name] = Exercise(name, [bodypart_dict[bp] for bp in el])

    return exercise_dict


def parse_program(
    contents: Dict[str, Any], ed: Dict[str, Exercise]
) -> List[Workout]:
    w = []
    for week, workouts in contents.items():
        for _, raw_work in workouts.items():
            workout = []
            for lift in raw_work:
                workout.append(
                    Lift(
                        ed[lift["exercise"]],
                        lift["weight"],
                        lift["sets"],
                        lift["reps"],
                    )
                )
            w.append(Workout(workout))
    return w


# with open("data/exercise.yaml", "r") as F:
#     exercises = get_exercise_dict(F.read())

# rich.print(exercises)
if __name__ == "__main__":
    exercise_dict = get_exercise_dict(load("data/exercise.yaml"))
    prog = parse_program(load("data/test_program.yaml"), exercise_dict)
    rich.print(prog)

    rich.print([get_total_sets(w) for w in prog])

