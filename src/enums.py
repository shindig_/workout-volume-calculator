from enum import Enum


class BodyPart(Enum):
    """UPPER"""

    TRICEPS = "triceps"
    BICEPS = "biceps"
    FOREARMS = "forearms"

    FRONT_DELT = "front delt"
    SIDE_DELT = "side delt"
    REAR_DELT = "rear delt"
    SHOULDERS = "shoulders"

    UPPER_BACK = "upper back"
    LOWER_BACK = "lower back"
    LATS = "lats"
    BACK = "back"

    CHEST = "chest"

    """ LOWER """
    QUADS = "quads"
    HAMSTRINGS = "hamstrings"
    CALVES = "calves"
    GLUTES = "glutes"

bodypart_dict = {v.value: v for _, v in BodyPart.__members__.items()}